package mailers

import (
	"os"
	"strconv"

	"github.com/gobuffalo/buffalo/mail"
	"github.com/gobuffalo/buffalo/render"
	"golang.org/x/crypto/bcrypt"
)

func SendSendCodes() error {
	m := mail.NewMessage()

	// fill in with your stuff:
	m.Subject = "Send Code"
	m.From = ""
	m.To = []string{}
	err := m.AddBody(r.HTML("send_code.html"), render.Data{})
	if err != nil {
		return err
	}
	return smtp.Send(m)
}

// SendCode Sending code by email
func SendCode(email string) (string, error) {
	var codeHashString string

	m := mail.NewMessage()

	// Create code fuction on range
	code := strconv.Itoa(createCode(1000, 9999))

	// fill in with your stuff:
	m.Subject = "Verification Code"
	m.From = os.Getenv("MAIL_USERNAME")
	m.To = []string{email}

	err := m.AddBody(r.HTML("send_code.html"), render.Data{"code": code})
	if err != nil {
		return codeHashString, err
	}

	err = smtp.Send(m)
	if err != nil {
		return codeHashString, err
	}

	// Generate email code hash.
	codehash, err := bcrypt.GenerateFromPassword([]byte(code), bcrypt.DefaultCost)
	if err != nil {
		return codeHashString, err
	}

	codeHashString = string(codehash)

	return codeHashString, nil
}
