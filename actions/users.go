package actions

import (
	"myapp/mailers"

	"github.com/gobuffalo/buffalo"
)

type Response struct {
	Code string
	Err  error
}

// UsersForgotPassword default implementation.
func UsersForgotPassword(c buffalo.Context) error {
	co, err := mailers.SendCode("joseavilacc1@gmail.com")

	var res Response
	res.Code = co
	res.Err = err

	return c.Render(200, r.JSON(res))
}
